a:
SELECT name FROM artists WHERE name LIKE "%D%";

b:
SELECT title FROM songs WHERE length > 230;

c:
SELECT albums.name, songs.title, songs.length FROM albums INNER JOIN songs ON albums.id = songs.albums_id;

d:
SELECT * FROM (artists LEFT JOIN albums ON artists.id = albums.artist_id) WHERE albums.name LIKE "%A%";

e:
SELECT * FROM albums ORDER BY name DESC LIMIT 4;

f:
SELECT * FROM albums JOIN songs ON albums.id = songs.albums_id ORDER BY name;
SELECT * FROM albums JOIN songs ON albums.id = songs.albums_id ORDER BY name DESC;